# README 

Extremely simple format converter for fastq to fasta.
It is actually one line script based on standard linux tool 'sed'.
See the source for the details ;-)

### What is this repository for? ###

This is a extremely simple sequence format converter for fastq to fasta.
It depends on sed which is one of the standard unix tools all unix-like system should provide.

### How do I get set up? ###

To set up:
`git clone git@bitbucket.org:tendo/fastq2fasta.git`

To execute:
 `fastq2fasta [fastq files] >file.fasta`


### Code

    #!/bin/sed -nf
    N;s/^@/>/p

### How it works

sed is a stream editor.
It reads one line from a file or standard input, process it, output and repeat over lines.

This script works as follows:

1. By -n option, default output behavior is suppressed 
1. By default behavior, a line is read into buffer
1. By N command, one line is added to buffer
1. By s command, @ sign at first is substituted for > in buffer
1. By p modifier, buffer is printed if substitution was made

Item 0 is for instructing shell for the executing sed.
By items 1 and 5, quality lines are siliently discarded.
Note that buffer is called 'pattern space' for sed.

### Contribution guidelines ###

Any improvements are welcome, but please avoid code explosion ;-)

### Who do I talk to? ###

endo-fastq2fasta@ibio.jp